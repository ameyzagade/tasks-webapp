// SETUP DATABASE CONNECTION

// require mongoose ODM
const mongoose = require('mongoose');


class Database  {
    
    constructor(db, username, passwd)   {
        this.host       = 'localhost';
        this.port       = '27017';
        this.db         = db;
        this.username   = username;
        this.pass       = passwd;

        this.options = {
            user:               this.username,
            pass:               this.pass,
            useNewUrlParser:    true,
        };

        // URL of local database
        this.url = `mongodb://${this.host}:${this.port}/${db}`;
    }

    connect_userdb()   {
        mongoose.connect(this.url, this.options)
        .then(() => {
            ;
        })
        .catch(err =>   {
            console.error('Error connecting to the database. ' + err);
        });
    }

    connect_taskdb()    {
        mongoose.connect(this.url, this.options)
        .then(() => {
            ;
        })
        .catch(err =>   {
            console.error('Error connecting to the database. ' + err);
        });
    }

    close_conn()    {
        mongoose.disconnect();
    }
}


// export database config
module.exports = Database;