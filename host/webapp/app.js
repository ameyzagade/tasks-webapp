const express = require('express');
const app = express();


// serve static frontend files
app.use(express.static(__dirname + '/client-side/'));


module.exports = app;