// handle for homepage
var homepage = angular.module('Homepage', []);

homepage.service('handler', ['$http', function($http)   {

    this.signUp = function(username, password)  {
        return $http.post('api/signup', {
            username:   username,
            password:   password
        });
    };

    this.signIn = function(username, password)    {
        return $http.post('api/signin', {
            username:   username,
            password:   password
        });
    };
}]);

// homepage.factory('AuthService', function($http)  {
//     var api = {};

//     api.postCreds = function(data)  {
//         return $http.post('/', data);
//     };

//     return api;
// });

// // handle for tasks
// var tasksApp = angular.module('Tasks', []);

// tasksApp.config(function($provide, $httpProvider)    {

//     $provide.factory('httpi', function($q, $log)    {
//         return {
//             'request': function(config) {
//                 return config;
//             },

//             'requestError': function(rejection) {
//                 return $q.reject(rejection);
//             },

//             'reponse': function(response)    {
                
//                 return response;
//             },

//             'responseError': function(rejection)    {
//                 return $q.reject(rejection);
//             }
//         };
//     });

//     $httpProvider.interceptors.push('httpi');
// });

//   // factory to interface with API    
//   tasksApp.factory('TaskManagerService', function($http)    {
        
//     var api = {};

//     api.addTask = function(data)    {
//         return $http.post('/tasks', data);  
//     };

//     api.fetchAll = function()   {
//         return $http.get('/tasks');
//     };
    
//     api.updateTask = function(taskID, data) {
//         return $http.put('/tasks/' + taskID, data);
//     };
    
//     api.deleteTask = function(taskID) {
//         return $http.delete('/tasks/' + taskID);
//     };

//     // return API object with properties corresponding to API call
//     return api;
// });