homepage.controller('HomepageController', function(handler, $scope)  {
    $scope.init = function()    {
        $scope.header = 'Sign In';
        $scope.showsignup = false;
        $scope.submitButton = 'Sign In';
        $scope.link = 'Need an account? ';
        $scope.linktext = 'Sign Up'
    };

    // $scope.submit = function()  {
        
    //     // username validation
    //     if ($scope.username.$valid)
    //     if (!RegExp(').test($scope.$username)) {
    //         $scope.$showusernamestatus = true;
    //         $scope.$usernamestatus = 'Username can only start with alphabets and contain aplhabets, underscores, numbers';
    //     } else {
    //         $scope.$showusernamestatus = false;
    //     }

    //     // password validation
    //     if ($scope.$password.length < 8)   {
    //         $scope.$showpasswordstatus = true;
    //         $scope.$passwordstatus = 'Password should be at least 8 characters long!';
    //     } else if ($scope.$password == $scope.$username)  {
    //         $scope.$showpasswordstatus = true;
    //         $scope.$passwordstatus = 'Password should be different from username!';
    //     } 
    //     else  {
    //         if (!RegExp('[a-z]').test($scope.$password))   {
    //             $scope.$showpasswordstatus = true;
    //             $scope.$passwordstatus = 'Password should contain atleast one small alphabet';
    //         } else if (!RegExp('[A-Z]').test($scope.$password))    {
    //             $scope.$showpasswordstatus = true;
    //             $scope.$passwordstatus = 'Password should contain atleast one block alphabet';
    //         } else if (!RegExp('[0-9]').test($scope.$password))   {
    //             $scope.$showpasswordstatus = true;
    //             $scope.$passwordstatus = 'Password should contain atleast one digit';
    //         } else  {
    //             $scope.$showpasswordstatus = false;
    //         }
    //     }
        
    //     if ($scope.showsignup)  {
    //         handler.signUp(username, password)
    //         .then(res => {
    //             console.log(res);
    //         })
    //         .catch(err =>  {
    //             console.log(err);
    //         });
    //     } else  {
    //         handler.signIn(username, password)
    //         .then(function(res) {
    //             console.log(res);
    //         })
    //         .catch(function(err)    {
    //             console.error(err);
    //         });
    //     }
    // }

    $scope.toggle = function()  {
       if ($scope.showsignup)   {
            $scope.init();
        } else   {
            $scope.showsignup = true;
            $scope.header = 'Sign Up';
            $scope.submitButton = 'Sign Up';
            $scope.link = 'Already have an account? ';
            $scope.linktext = 'Sign In'
        }
    }
});