tasksApp.controller('MainController', function($scope)  {
    var dateToday = new Date();

    // day and month arrays
    var daysInWeek    = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var monthsInYear  = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    // set day and date in view
    $scope.day = daysInWeek[dateToday.getDay()];
    $scope.date = daysInWeek[dateToday.getDay()] + ', ' + dateToday.getDate() + ' ' +  monthsInYear[dateToday.getMonth()] + ' ' + dateToday.getFullYear();
    $scope.count = 0;

    $scope.close = function()   {
        $scope.conflict = "";
        $scope.hidden = true;
    }
});

tasksApp.controller('taskController', function(TaskManagerService, $scope)   {
    $scope.$parent.add = function() {
        TaskManagerService.addTask({
            taskname:       $scope.$parent.taskname,
            checked:        false
        })
        .then(function(res)    {
            $scope.$parent.taskname = '';
            $scope.tasks.push(res.data.data);
            $scope.$parent.count++;
        })
        .catch(function(err)   {
            switch(err.status)  {
                case 400:
                    $scope.$parent.hidden = false;
                    $scope.$parent.conflict = err.data.message;
                    $scope.$parent.taskname = '';
                    handleConflict(err.data.message);
                    break;
                case 500:
                    handleConflict(err.data.message);
                    break;
                default:
                    break;
            }
        });
    };

    TaskManagerService.fetchAll()
    .then(function(res)    {
        $scope.tasks = res.data.allTasks;
        $scope.$parent.count = res.data.allTasks.length;
    })
    .catch(function(err)   {
        handleConflict(err.data.message);
    });

    $scope.edit = function(task)    {
        var newTask = prompt("Enter a new taskname");

        var data = {
            taskname:   newTask,
            checked:    task.checked
        };

        TaskManagerService.updateTask(task._id, data)
        .then(function(res)    {
            task.name = newTask;
        }).catch(function(err)  {
            switch(err.status)  {
                case 404:
                    handleConflict('Task not found');
                    break;
                case 406:
                    handleConflict('Task body empty');
                    break;
                case 500:
                    $scope.$parent.hidden = false;
                    $scope.$parent.conflict = "Duplicate task name " + newTask;
                    handleConflict("Duplicate task name " + newTask);
                    break;
                default:
                    break;
            }
        });
    }

    $scope.del = function(taskid, index) {
        TaskManagerService.deleteTask(taskid)
        .then(function(res)    {
            $scope.tasks.splice(index, 1);
            $scope.$parent.count--;
        })
        .catch(function(err)   {
            handleConflict(err.data.message);
        });
    };

    $scope.toggleCheck = function(task)  {
        var data = {
            taskname:       task.task,
            checked:        !task.checked
        };

        TaskManagerService.updateTask(task._id, data)
        .then(function(res)    {
            task.checked = data.checked;
        })
        .catch(function(err)   {
            handleConflict(err.data.message);
        });
    };

    function handleConflict(message)    {
        $scope.$parent.hidden = false;
        $scope.$parent.conflict = message;
    }
});