// nodejs server

const express = require('express');
const cookieParser = require('cookie-parser');
const uuid = require('uuid');
const session = require('express-session');

// express root app
const app = express();


// express sub-apps
const api = require('../api/app.js');
const webapp = require('../webapp/app.js');


// configure node host and port
const hostname = 'localhost';
const port = process.env.port || 3000;

app.use(session({
    genid: (req) => {
        return uuid();
    },
    secret:     'kung fu panda',
    resave:     false,
    saveUninitialized: true
}));


app.use('/api', api);
app.use('/', webapp);



app.listen(port, hostname, () =>    {
    console.log('Server started');
});