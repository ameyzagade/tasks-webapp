const express = require('express');

const routes = require('./routes/app.routes.js');

// API express app
const app = express();


// use built-in json middleware
// to parse JSON request and response body
app.use(express.json());


// Routes
app.use('/', routes);

module.exports = app;