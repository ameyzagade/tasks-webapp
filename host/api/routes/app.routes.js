// routes to access the API

const express = require('express');
const router = express.Router();

// controllers
const homepage = require('../controllers/home.controller.js');
const task = require('../controllers/task.controller.js');

// add an user to database /api/signup
router.post('/signup', homepage.signUp);

// sign in user to the database /api/signin
router.post('/signin', homepage.signIn);

 // create a task   /api/tasks
router.post('/tasks', task.create);

// read all tasks   /api/tasks
router.get('/tasks/:key', task.read);

// update a task    /api/tasks/:taskID
router.put('tasks/:taskID', task.update);

// delete a task    /api/tasks/:taskID
router.delete('tasks/:taskID', task.del);

module.exports = router;