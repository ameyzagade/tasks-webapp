// API controller to manage request and responses to home

// USER CREDENTIALS MODEL
const usersdb = require('../models/users.model.js');

// DATABASE CONFIGURATION OBJECT
const dbconfig = require('../../../lib/db/db.config.js');


const signUp = (req, res) =>  {

    // regex to validate the input username
    // username should start only with alphabets and can contain alphanumeric characters
    let regex = RegExp('^[a-zA-Z0-9]+[_a-zA-Z0-9]*$');


    if (!req.body)   {
        return res.status(400).json({
            message:            'POST body cannot be empty'
        });
    }
    // validate username with regex
    else if (regex.test(req.body.username))    {

        // connect to the user details database
        let userdbaccess = new dbconfig('users', 'admin-user', 'pass123');
        userdbaccess.connect_userdb();
        
        // check if the login credentials already exist in the database,
        let query = usersdb.findOne({username:  req.body.username});
        let promise = query.exec();

        promise
        .then(users =>   {

            if (!users)  {
    
                // if none user match, save new credentials to database
                const data = new usersdb({
                    username        : req.body.username,
                    password        : req.body.password,
                    token           : '1132'
                });
    
                
                // save user credentials to the database
                let promise = usersdb.create(data);
                
                promise.then((data) =>    {
                    userdbaccess.close_conn();
                    return res.status(200).json({
                        message:        'User registration successful',
                        token:          data.token
                    });
                })
                .catch(err =>   {
                    userdbaccess.close_conn();
                    return res.status(500).json({
                        message:        err.message || 'Some error occured while signup!'
                    });
                });
            } else {
                userdbaccess.close_conn();
                return res.status(400).json({
                    message:    `username ${users.username} already exists`
                });
            }
        })
        .catch(err =>   {
            userdbaccess.close_conn();
            return res.status(500).json({
                message:            err.message || 'Some error occured while signup!'
            });           
        });
    }
    else {
        return res.status(400).json({
            message:                'Invalid username'
        });
    }
};


const signIn = (req, res) =>  {
  
    // regex to validate the input username
    // username should start only with alphabets and can contain alphanumeric characters
    let regex = RegExp('^[a-zA-Z0-9]+[_a-zA-Z0-9]*$');


    if(!req.body)   {
        return res.status(400).json({
            message:        'POST body cannot be empty'
        });
    }
    // validate username with regex
    else if (regex.test(req.body.username))    {

        // connect to the user details database
        let userdbaccess = new dbconfig('users', 'admin-user', 'pass123');
        userdbaccess.connect_userdb();
        

        // check if the login credentials already exist in the database,
        let query = usersdb.findOne({username : req.body.username});
        let promise = query.exec();

        promise
        .then(user =>   {
            
            // compare username and password hash
            if (req.body.username == user.username) {
                user.comparePassword(req.body.password, function(err, result)  {
                    if (err)    {
                        userdbaccess.close_conn();
                        return res.status(500).json({
                            message:            'Error occured while authentication'
                        });
                    }
                    
                    if (result) {
                        userdbaccess.close_conn();
                        return res.status(200).json({
                            message:        'Authentication successful',
                            token:          data.APItoken
                        });
                    } 

                    // if credentials dont match
                    userdbaccess.close_conn();
                    return res.status(400).json({
                        message:            'Authentication failed'
                    });
                });
            }
        })
        .catch(err =>   {
            userdbaccess.close_conn();
            return res.status(404).json({
                message:                    'Username not found'
            });
        });
    }
    else    {
        return res.status(400).json({
            message:                        'Invalid username'
        });
    }
};

module.exports = {
    signUp, signIn
};