// CONTROLLER TO QUERY ON DATABASE

// USER CREDENTIALS MODEL
const usersdb = require('../models/users.model.js');

// import model from sibling directory
const taskdb = require('../models/tasks.model.js');

// DATABASE CONFIGURATION OBJECT
const dbconfig = require('../../../lib/db/db.config.js');

// create task and save
const create = (req, res) =>    {

    // validate request
    if (!req.body.taskname)  {
        return res.status(400).json({
            message:        'Task body cannot be empty!'
        });
    } else  {
        let taskdbaccess = new dbconfig('tasks', 'admin-amey', '12#456');
        taskdbaccess.connect_taskdb();
        
        // check task exists
        let query = taskdb.findOne({ task : req.body.taskname });
        let promise = query.exec();

        promise
        .then(data => {
            // check if findOne returns null
            if (data != null)  {
                taskdbaccess.close_conn();
                return res.status(400).json({
                    message:    `Task \'${req.body.taskname}\' already exists!`,
                });
            }

            // create task
            const task = new taskdb({
                task:           req.body.taskname,
                checked:        req.body.checked,
            });

            // save task to database
            task.save()
            .then(data =>   {
                taskdbaccess.close_conn();
                return res.status(200).json({
                    message:    `Task \'${req.body.taskname}\' added successfully!`,
                    data:       data
                });
            }).catch(err => {
                taskdbaccess.close_conn();
                return res.status(500).json({
                    message:    err.message || 'Some error occured while creating the task!'
                });
            });
        }).catch(err => {
            taskdbaccess.close_conn();
            return res.status(500).json({
                message:        err.message || 'Some error occured while creating the task!'
            });
        });
    }
}


// retrieve all tasks
const read = (req, res) =>   {
    
    let userdbaccess = new dbconfig('users', 'admin-user', 'pass123');
    userdbaccess.connect_userdb();

    // check if api key exists
    let query = usersdb.findOne({APItoken: req.params.key});
    let promise = query.exec();

    promise
    .then(user =>   {
        let taskdbaccess = new dbconfig('tasks', 'admin-amey', '12#456');
        taskdbaccess.connect_taskdb();
    
        let query = taskdb.find({APItoken: req.params.key});
        let promise = query.exec();
    
        promise
        .then(allTasks =>   {
            taskdbaccess.close_conn();
            res.status(200).json({
                message:        'Tasks retrieved',
                allTasks:       allTasks,
            });
        }).catch(err => {
            taskdbaccess.close_conn();
            res.status(500).json({
                message:        err.message || 'Some error occured while retrieving the tasks!'
            });
        });
    })
    .catch(err =>   {
        userdbaccess.close_conn();
        taskdbaccess.close_conn();
        return res.status('404').json({
            message:                'Invalid API key'
        });
    });
}


// update task with ID and name
const update = (req, res) =>    {

    // validate request
    if (!req.body.taskname)  {
        return res.status(406).json({
            message:        'Task body cannot be empty'
        });
    }

    let taskdbaccess = new dbconfig('tasks', 'admin-amey', '12#456');
    taskdbaccess.connect_taskdb();


    taskdb.findByIdAndUpdate(req.params.taskID, {$set: {task: req.body.taskname, checked: req.body.checked}}, {new: true})
    .then(task =>   {
        taskdbaccess.close_conn();
        res.status(200).json({
            message:        `Task with id \'${req.params.taskID}\' updated`
        });
    }).catch(err => {
        if (err.kind === 'ObjectId')    {
            taskdbaccess.close_conn();
            return res.status(404).json({
                message:    `Task with id \'${req.params.taskID}\' not found`
            });
        }

        taskdbaccess.close_conn();
        return res.status(500).json({
            message:        err.message || 'Some error occured while updating the task!'
        });
    });
}


// delete task with ID
const del = (req, res) =>   {

    let taskdbaccess = new dbconfig('tasks', 'admin-amey', '12#456');
    taskdbaccess.connect_taskdb();

    taskdb.findByIdAndDelete(req.params.taskID)
    .then(data =>   {
        taskdbaccess.close_conn();
        return res.status(200).json({
            message:        `Task with id \'${req.params.taskID}\' deleted successfully`
        });
    }).catch(err => {
        if (err.kind === 'ObjectId')    {
            taskdbaccess.close_conn();
            return res.status(404).json({
                message:    `Task with id \'${req.params.taskID}\' not found`
            });
        }

        taskdbaccess.close_conn();
        return res.status(500).json({
            message:        err.message || 'Some error occured while deleting the task!'
        });
    });
}


module.exports = {
    create, read, update, del
}