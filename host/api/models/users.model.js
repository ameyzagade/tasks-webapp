// USER SCHEMA DEFINTION
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

// salt factor to hash
const SALT_WORK_FACTOR = 10;

// token config
const tokenconfig = require('../../../lib/token/token.config.js');


// USER MODEL
const userSchema = mongoose.Schema(
    {
        username:   {
            type:           String,             
            required:       true,                  
            index:  {
                unique:     true
            },                                      
            minlength:      1,                      
        },
        password:    {
            type:           String,
            default:        '',
            required:       true
        },
        token:   {
            type:           String,
            required:       true,
            unique:         true
        }
    },
    {
        collection: 'user_data'
    }
);


// hash password before saving
userSchema.pre('save', function(next)    {

    var user = this;

    // only hash if password is modified
    if (!user.isModified('password'))
        return next;
    
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt)    {
        if (err)
            return err;
        
        // hash cleartext password
        bcrypt.hash(user.password, salt, function(err, hash)    {
            if (err)
                return err;

            // save hashed password and token
            user.password = hash;

            user.token = jwt.sign(
                {
                    username:           user.username,
                },
                tokenconfig.secret,
                {
                    expiresIn:          864000
                }
            );
            
            next();
        });
    });
});

userSchema.methods.comparePassword = (password, cb) =>  {
    bcrypt.compare(password, this.password, function(err, res)  {
        if (err)
            return err;

        cb(null, res);
    });
};


module.exports = mongoose.model('Users', userSchema);