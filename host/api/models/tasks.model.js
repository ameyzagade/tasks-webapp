// Tasks SCHEMA DEFINITION
const mongoose = require('mongoose');


// TASK MODEL
const taskSchema = mongoose.Schema(
    {
        taskname:    {
            type:       String,             
            required:   true,               
            unique:     true,               
            minlength:  1,                  
        },
        checked:    {
            type:       Boolean,
            required:   true,
            default:    false,              
        },
        isUpdated:  {
            type:       Boolean,
            default:    false
        },
        token:   {
            type:       String,
            required:   true,
            unique:     true,
        }
    },
    {
        timestamps:     true               
    },
    {
        collection:     'tasks_collection'
    }
);


module.exports = mongoose.model('Tasks', taskSchema);